package com.mini.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mini.Dto.AddressDto;
import com.mini.Dto.EmployeeDto;
import com.mini.service.EmployeeService;
import com.mini.service.AddressService;

@RestController
public class EmployeeController {
	
	// OneToOne Practice
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private AddressService productService;
	
	@PostMapping("/saveEmployees")
	public ResponseEntity<EmployeeDto> saveEmployees(@RequestBody EmployeeDto employeeDto ) {
				EmployeeDto employeeDto2		= employeeService.saveEmployees(employeeDto );
					return ResponseEntity.ok(employeeDto2);
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<EmployeeDto>> findAllEmployess(){
						List<EmployeeDto> dtos	=	employeeService.getAllEmployees();
						return ResponseEntity.ok(dtos);
	}
	
	@GetMapping("getBy/{name}")
	public ResponseEntity<EmployeeDto> getByName(@PathVariable  String name){
			EmployeeDto dtos = employeeService.getByName(name);
			return ResponseEntity.ok(dtos);
	}
	
	@GetMapping("getAddress/{name}")
	public List<AddressDto> getAddress( @PathVariable String name) {
		
		List<AddressDto> addressDtos	= productService.findByAddress(name);
		return addressDtos;		
	}
}
