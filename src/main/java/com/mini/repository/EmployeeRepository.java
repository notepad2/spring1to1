package com.mini.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mini.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>  {

	Employee findByEmpName(String name);

}
