package com.mini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mini.model.Address;

public interface AddressRepository extends JpaRepository<Address,Integer> {

	List<Address> findByaddressType(String name);
			
}
