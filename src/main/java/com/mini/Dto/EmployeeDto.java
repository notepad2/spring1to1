package com.mini.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class EmployeeDto {

	private int empId;
	private String empName;
	private long empAge;
	
	private AddressDto addressDto;
}
