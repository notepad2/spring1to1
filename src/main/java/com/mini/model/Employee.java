package com.mini.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data

@Table(name ="EmpTable")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="empId")
	private int empId;
	private String empName;
	private long empAge;
	
	// Foreign Key Address  Will be Automatically added to the Employee table 
	// with the name (Model Name )Address_Adress Id(This One address table pk)
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_Adress Id")  
	private Address address;
}
