package com.mini.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mini.Dto.EmployeeDto;

@Service
public interface EmployeeService {

	EmployeeDto saveEmployees(EmployeeDto employeeDto);

	List<EmployeeDto> getAllEmployees();

	EmployeeDto getByName(String name);

}
