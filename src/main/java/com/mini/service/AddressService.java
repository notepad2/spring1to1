package com.mini.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mini.Dto.AddressDto;

@Service
public interface AddressService {

	List<AddressDto> findByAddress(String name);

}
