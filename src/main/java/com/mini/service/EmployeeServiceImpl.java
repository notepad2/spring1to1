package com.mini.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.mini.Dto.AddressDto;
import com.mini.Dto.EmployeeDto;
import com.mini.model.Address;
import com.mini.model.Employee;
import com.mini.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	
	@Override
	public EmployeeDto saveEmployees(EmployeeDto employeeDto) {
			
		ConvertingProcess process = new ConvertingProcess();
		
		Employee employee = new Employee();
		employee.setEmpName(employeeDto.getEmpName());
		employee.setEmpAge(employeeDto.getEmpAge());
		employee.setAddress(process.convertToEntityAd(employeeDto.getAddressDto()) );
			Employee employee2	=employeeRepository.save(employee);
			
		EmployeeDto dto = new EmployeeDto();
		dto.setEmpName(employee2.getEmpName());
		dto.setEmpAge(employee2.getEmpAge());
		dto.setAddressDto(process.convertToDto(employee2.getAddress()) );
		
		return dto;
	}

	@Override
	public List<EmployeeDto> getAllEmployees() {
		
		List<Employee> employees = employeeRepository.findAll();
		List<EmployeeDto> dtos = new ArrayList<>();
		
		ConvertingProcess process = new ConvertingProcess();
		
		for (  Employee employee: employees) {
			
			EmployeeDto dto = new EmployeeDto();
			dto.setEmpId(employee.getEmpId());
			dto.setEmpName(employee.getEmpName());
			dto.setEmpAge(employee.getEmpAge());
			dto.setAddressDto(process.convertToDto(employee.getAddress()) );
			
			dtos.add(dto);
			
		}
		return dtos;
	}
	
	@Override
	public EmployeeDto getByName(String name) {
			
		Employee employee = employeeRepository.findByEmpName(name);
		
		EmployeeDto dto = new EmployeeDto();
		
		ConvertingProcess process = new ConvertingProcess();
		dto.setEmpId(employee.getEmpId());
		dto.setEmpName(employee.getEmpName());
		dto.setEmpAge(employee.getEmpAge());
		dto.setAddressDto(process.convertToDto(employee.getAddress()));
		
		return dto;
	}
}

@Component
class ConvertingProcess{
	
	public AddressDto convertToDto(Address address ) {
		AddressDto addressDto = new AddressDto();
		
		addressDto.setAdId(address.getAdId());
		addressDto.setCity(address.getCity());
		addressDto.setAddressType(address.getAddressType());
		
		return addressDto;
	}
	
	public Address convertToEntityAd(AddressDto addressDto ) {
		
		Address address = new Address(addressDto.getAdId(), addressDto.getCity(), addressDto.getAddressType());
		return address;
	}
}
