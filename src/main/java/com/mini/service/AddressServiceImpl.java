package com.mini.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mini.Dto.AddressDto;
import com.mini.model.Address;
import com.mini.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository addressRepository;

	@Override
	public List<AddressDto> findByAddress(String name) {

		List<Address> addresses = addressRepository.findByaddressType(name);

		List<AddressDto> addressDtos = new ArrayList<>();

		for (Address address : addresses) {
			AddressDto addressDto1 = new AddressDto();
			
			addressDto1.setAdId(address.getAdId());
			addressDto1.setCity(address.getCity());
			addressDto1.setAddressType(address.getAddressType());

			addressDtos.add(addressDto1);
		}
		return addressDtos;
	}
}
